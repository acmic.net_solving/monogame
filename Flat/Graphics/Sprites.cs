﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Flat.Graphics
{
    public sealed class Sprites : IDisposable //사용이 끝난뒤 자동으로 메모리 해제를 시키는 인터페이스상속
    {
        private bool isDisposed;
        private Game game;
        private SpriteBatch sprites;
        private BasicEffect effect; //효과를 만들수 있으니?

        public Sprites(Game game)
        {
            if (game is null) throw new ArgumentNullException("game");

            this.game = game;
            this.isDisposed = false;
            this.sprites = new SpriteBatch(this.game.GraphicsDevice);
            this.effect = new BasicEffect(this.game.GraphicsDevice);
            this.effect.FogEnabled = false; //2D게임 텍스처 활성에는 안개 필요 X
            this.effect.TextureEnabled = true;
            this.effect.LightingEnabled = false; //조명 필요X
            this.effect.VertexColorEnabled = true; //정점 색상변경필요.
            this.effect.World = Matrix.Identity; //행렬 항등식으로 설정?
            this.effect.Projection = Matrix.Identity; //1)
            this.effect.View = Matrix.Identity; //2)    //1과 2는 카메라가 제대로 작동하는지에따라 변경될것?
        }

        public void Dispose()
        {
            if (this.isDisposed) return;

            this.effect?.Dispose();
            this.sprites?.Dispose();
            this.isDisposed = true;
        }

        public void Begin(bool isTextureFileteringEnabled)
        {
            SamplerState sampler = SamplerState.PointClamp;
            if (isTextureFileteringEnabled) sampler = SamplerState.LinearClamp;

            Viewport vp = this.game.GraphicsDevice.Viewport;
            this.effect.Projection = 
                Matrix.CreateOrthographicOffCenter(0, vp.Width, 0, vp.Height, 0f, 1f);

            this.sprites.Begin(blendState: BlendState.AlphaBlend, samplerState: sampler,
                rasterizerState: RasterizerState.CullNone, effect: this.effect);
        }

        public void End()
        {
            this.sprites.End();
        }

        //origin:원점? position:위치 color:정점색상
        public void Draw(Texture2D texture, Vector2 origin, Vector2 position, Color color)
        {
            this.sprites.Draw(texture, position, null, color, 0f, origin, 1f, 
                SpriteEffects.FlipVertically, 0f); //FlipVertically -> 수직뒤집기
        }

        public void Draw(Texture2D texture, Rectangle? sourceRectangle, 
            Vector2 origin, Vector2 position, float rotation, Vector2 scale, Color color)
        {
            this.sprites.Draw(texture, position, sourceRectangle,
                color, rotation, origin, scale, SpriteEffects.FlipVertically, 0f);
        }


        public void Draw(Texture2D texture, Rectangle? sourceRectangle, 
            Rectangle destinationRectangle, Color color)
        {
            this.sprites.Draw(texture, destinationRectangle, sourceRectangle, color, 
                0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
        }

    }
}