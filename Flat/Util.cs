﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat
{
    public static class Util
    {
        public static int Clamp(int value, int min, int max)
        {
            if(min > max)
            {
                throw new ArgumentOutOfRangeException("최솟값과 최댓값이 올바르지 않습니다.");
            }

            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

        public static float Clamp(float value, float min, float max)
        {
            if (min > max)
            {
                throw new ArgumentOutOfRangeException("최솟값과 최댓값이 올바르지 않습니다.");
            }

            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

    }
}
