﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Flat;
using Flat.Graphics;
using Flat.input;
using System;

namespace MonoGamePalTester
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private Sprites sprites;
        private Texture2D texture;
        private Screen screen;

        private float x = 32;

        public Game1()
        {
            this.graphics = new GraphicsDeviceManager(this);
            this.graphics.SynchronizeWithVerticalRetrace = true;

            this.Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
            this.IsFixedTimeStep = true;
        }

        protected override void Initialize()
        {
            this.graphics.PreferredBackBufferWidth = 400;
            this.graphics.PreferredBackBufferHeight = 800;
            this.graphics.ApplyChanges();

            this.sprites = new Sprites(this);
            this.screen = new Screen(this, 640, 480);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.texture = this.Content.Load<Texture2D>("Box"); //텍스쳐유형을 ""로 제시
        }

        protected override void Update(GameTime gameTime)
        {
            FlatKeyboard keyboard = FlatKeyboard.Instance;
            keyboard.Update();

            if (keyboard.IsKeyClicked(Keys.Escape))
                this.Exit();

            if (keyboard.IsKeyClicked(Keys.Right))
            {
                x += 32f;
                Console.WriteLine("Right arrow clicked.");
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            this.screen.Set();
            this.GraphicsDevice.Clear(Color.CornflowerBlue);


            Viewport viewport = this.GraphicsDevice.Viewport;

            this.sprites.Begin(false); //지금은 텍스처 필터링이 필요 X
            this.sprites.Draw(texture, null, new Rectangle((int)x, 32, 512, 256), Color.White);                         
            this.sprites.End(); //종료시 전체 배치를 가져와서 한번에 그래픽카드에 제출? 
                                //이것이 스프라이트를 하나씩 그래픽카드에 전달하는것보다 효율적

            this.screen.UnSet();
            this.screen.Present(this.sprites);

            base.Draw(gameTime);
        }
    }
}